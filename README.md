# Covid Api
***
## Installation:
### Clone GitLab repository

```
git clone https://gitlab.com/Ivaylo-Yordanov222/covid-api.git
cd covid-api
```

### Configure database connection
```
server.port=[SERVER_PORT (default 8085)] 

spring.datasource.url=jdbc:mysql://localhost:3306/[SCHEMA_NAME]?autoreconnect=true&createDatabaseIfNotExist=true&characterEncoding=utf8
spring.datasource.username=[DB_USERNAME]
spring.datasource.password=[DB_PASSWORD]

external.covid.api.url=[EXTERNAL_API_URL]
cron.job.covid.delay=[DELAY (in miliseconds, default 600000 ms = 10 min)] 
```
***

## Name
Covid Microservice
***
## Description
Simple Spring Boot microservice that consumes https://api.covid19api.com/summary and uses endpoint for COUNTRY/COUNTRYCODE to show covid statistics about desired country.
***
## Dependency list
- [ ] WEB CLIENT 
- [ ] MYSQL  
- [ ] HIBERNATE
- [ ] JPA
- [ ] LOMBOK
- [ ] SCHEDULING (Cron Job)
- [ ] JUNIT
- [ ] MOCKITO
***

## API usage
- [ ] GET request -> http://localhost:8085/api/v1/country/{countryCode} 
  * country code must be uppercase only 2 characters long 
- [ ] GET request -> http://localhost:8085/api/v1/allCovidData
***

## Authors and acknowledgment
### Ludogorie Soft.
- [ ] Ivaylo Yordanov
- [ ] Aleksander Pavlov
- [ ] Emiliyan Kadiysky